package io.codex.service.dto.core;

import lombok.Builder;
import lombok.Value;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;

import java.io.Serializable;

@Value
@Builder
public class GeoLocationDTO implements Serializable {

    private Double longitude;

    private Double latitude;

    public Point toPoint(){
        return new GeometryFactory().createPoint(new Coordinate(longitude, latitude));
    }

}
