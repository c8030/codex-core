package engine;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import model.OpenApiSpecification;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;

@Component
public class Engine {

    public void kickStart() throws IOException {

        Resource resource = new ClassPathResource("openapi-spec.json");

        OpenApiSpecification apiSpecification = new Gson().fromJson( new JsonReader(new FileReader(resource.getFile())),OpenApiSpecification.class);

        MustacheFactory mf = new DefaultMustacheFactory();
        Mustache m = mf.compile("todo.mustache");

        StringWriter writer = new StringWriter();
        m.execute(writer, apiSpecification).flush();
        String html = writer.toString();

        System.out.println(html);
    }

}
