package model;

import lombok.Data;

import java.util.Map;

@Data
public class RequestBodyContent {

    private Map<String, SchemaType> schema;
}
