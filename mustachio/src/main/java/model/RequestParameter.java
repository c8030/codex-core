package model;

import lombok.Data;

@Data
public class RequestParameter {

    private String name;

    private String in;

    private String description;

    private Boolean required;

    private String style;

    private SchemaType schema;

}
