package model;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class RestEndpoint {

    private List<String> tags;

    private String summary;

    private String operationId;

    private List<RequestParameter> parameters;

    private RequestBody requestBody;

    private Map<String, Response> responses;

    private List<Security> security;

}
