package model;

import lombok.Data;

@Data
public class Info {

    private String title;

    private String description;

    private String termsOfService;

    private Contact contact;

    private License license;

    private String version;
}
