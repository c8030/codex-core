package model;

import lombok.Data;

@Data
public class SchemaType {

    private String type;

    private Item items;

    private String format;

}
