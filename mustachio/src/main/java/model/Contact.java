package model;

import lombok.Data;

@Data
public class Contact {

    private String name;

    private String url;

    private String email;
}
