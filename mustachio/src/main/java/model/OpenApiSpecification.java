package model;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class OpenApiSpecification {

    private String openapi;

    private Info info;

    private List<Server> servers;

    private List<Tag> tags;

    private Map<String, Map<String, RestEndpoint>> paths;
}
