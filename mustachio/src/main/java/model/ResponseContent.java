package model;

import lombok.Data;

@Data
public class ResponseContent {

    private SchemaType schema;

}
