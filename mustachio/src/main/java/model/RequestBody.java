package model;

import lombok.Data;

import java.util.Map;

@Data
public class RequestBody {

    private RequestBodyContent content;

}
