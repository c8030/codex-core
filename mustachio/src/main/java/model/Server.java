package model;

import lombok.Data;

@Data
public class Server {

    private String url;

    private String description;

}
