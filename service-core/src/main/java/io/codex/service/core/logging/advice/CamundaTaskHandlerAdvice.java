package io.codex.service.core.logging.advice;

import io.codex.service.core.context.RequestContext;
import io.codex.service.core.context.RequestContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.camunda.bpm.client.task.ExternalTask;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class CamundaTaskHandlerAdvice {
    private static final String CAMUNDA_WORKFLOW = "camunda_workflow";
    private static final String BUSINESS_KEY = "business_key";
    private static final String WORKFLOW_INSTANCE = "workflow_instance";
    private static final String EXECUTION_CHANNEL = "execution_channel";

    @Pointcut("args(org.camunda.bpm.client.task.ExternalTask, org.camunda.bpm.client.task.ExternalTaskService)")
    public void isValidMethod(){}

    @Pointcut("@within(org.camunda.bpm.client.spring.annotation.ExternalTaskSubscription) || @annotation(org.camunda.bpm.client.spring.annotation.ExternalTaskSubscription)")
    public void isCamundaTaskHandler(){}

    @Around("isCamundaTaskHandler() && isValidMethod()")
    public void addCamundaLoggingContext(final ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        log.trace("Adding Camunda Context to logback MDC ...");

        try {
            ExternalTask externalTask = (ExternalTask) proceedingJoinPoint.getArgs()[0];
            //Add Camunda context
            MDC.put(EXECUTION_CHANNEL, CAMUNDA_WORKFLOW);
            MDC.put(BUSINESS_KEY, externalTask.getBusinessKey());
            MDC.put(WORKFLOW_INSTANCE, externalTask.getActivityInstanceId());

            //Set Thread Context
            RequestContextHolder.setContext(RequestContext.of(externalTask.getBusinessKey()));

            //execute method
            proceedingJoinPoint.proceed();
        } finally {
            //Clear Context once method execution is done
            MDC.clear();
        }
    }

}
