package io.codex.service.core.exceptions.platform;

import io.codex.service.core.exceptions.base.InvalidInputException;
import org.springframework.http.HttpStatus;

public class InvalidRequestException extends InvalidInputException {

    public InvalidRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidRequestException(String message) {
        super(message);
    }

    @Override
    public String getCode() {
        return super.getCode();
    }

    @Override
    public HttpStatus getStatus() {
        return super.getStatus();
    }
}
