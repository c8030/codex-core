package io.codex.service.core.security;

import io.codex.service.core.exceptions.platform.InvalidRequestException;
import io.codex.service.core.utils.CodexOptional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Pattern;

@Slf4j
public class AuthenticationFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {

        CodexOptional<String> authorizationHeader = CodexOptional.ofNullable(request.getHeader("Authorization"));

        authorizationHeader
                .ifPresent(header -> setSecurityContext(header))
                .orElseThrow(() -> new InvalidRequestException("Authorization header not present"));

        chain.doFilter(request,response);

    }

    private void setSecurityContext(String authHeader) {
        if (authHeader.startsWith("Bearer")) {
            setUserSecurityContext(authHeader);
        }
    }

    private void setUserSecurityContext(String header) {
        validateHeaderPattern(SecurityConstants.AUTHORIZATION_HEADER_BEARER_FORMAT, header);
        CodexUser user = JwtUtil.getUserFromToken(header.substring(7));

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user,null, user.getAuthorities()));

        log.info(user.toString());
    }

    private void validateHeaderPattern(String regex, String header) {
        Pattern pattern = Pattern.compile(regex);
        if (!pattern.matcher(header).matches()) {
            throw new InvalidRequestException("Invalid header format !!");
        }
    }

}
