package io.codex.service.core.context;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RequestContextHolder {
    private static ThreadLocal<RequestContext> requestContextThreadLocal = ThreadLocal.withInitial(() -> RequestContext.of(null));

    public static void setContext(RequestContext requestContext){
        log.trace("Setting Request Context: {}", requestContext);
        requestContextThreadLocal.set(requestContext);
    }

    public static String getCorrelationId(){
        return requestContextThreadLocal.get().getCorrelationId();
    }

}
