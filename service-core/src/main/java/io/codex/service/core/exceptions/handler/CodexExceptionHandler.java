package io.codex.service.core.exceptions.handler;

import io.codex.service.core.exceptions.base.BaseUncheckedException;
import io.codex.service.core.exceptions.error.ErrorDTO;
import io.codex.service.core.exceptions.error.PlatformErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@RestControllerAdvice
public class CodexExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<ErrorDTO> handleUnknownException(Throwable ex){
        ErrorDTO errorDTO = new ErrorDTO(PlatformErrorCode.PLATFORM_ERR_UNKNOWN.getCode(), "Server Error");
        this.logException(errorDTO, ex);
        return new ResponseEntity(errorDTO, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(BaseUncheckedException.class)
    public ResponseEntity<ErrorDTO> handleUncheckedException(BaseUncheckedException ex){
        ErrorDTO errorDTO = new ErrorDTO(ex.getCode(), ex.getMessage());
        this.logException(errorDTO, ex);
        return new ResponseEntity(errorDTO, ex.getStatus());
    }

    private void logException(ErrorDTO errorDTO, Throwable ex){
        log.error("Error Response: {}", errorDTO);
        log.error("Exception: {}", ex);
    }

}
