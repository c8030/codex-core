package io.codex.service.core.exceptions.base;

import io.codex.service.core.exceptions.error.PlatformErrorCode;
import org.springframework.http.HttpStatus;

public abstract class UnauthenticatedException extends BaseUncheckedException implements BaseException {

    public UnauthenticatedException(String message, Throwable cause) {
        super(message, PlatformErrorCode.PLATFORM_ERR_FORBIDDEN, cause);
    }

    public UnauthenticatedException(String message) {
        super(message, PlatformErrorCode.PLATFORM_ERR_FORBIDDEN);
    }

    @Override
    public String getCode() {
        return super.getCode();
    }

    @Override
    public HttpStatus getStatus() {
        return super.getStatus();
    }

}
