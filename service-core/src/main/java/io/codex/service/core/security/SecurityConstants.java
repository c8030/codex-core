package io.codex.service.core.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SecurityConstants {

    public static final String SIGN_UP_URL = "/users/record";
    public static String KEY;
    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String AUTHORIZATION_HEADER_BEARER_FORMAT = "^Bearer [a-zA-Z0-9]*.[a-zA-Z0-9]*.[a-zA-Z0-9-_]*$";
    public static Long EXPIRATION_TIME;
    public static final String LOGIN_PATTERN = "^[a-zA-Z]{1}[a-zA-Z_.0-9]*@[a-zA-Z]{1}[a-zA-Z_.0-9]*$";

    @Autowired
    public SecurityConstants(@Value("${auth.jwt.secret}") String key,@Value("${auth.jwt.ttlInMinutes}") long ttlInMinutes) {
        KEY = key;
        EXPIRATION_TIME = ttlInMinutes;
    }
}
