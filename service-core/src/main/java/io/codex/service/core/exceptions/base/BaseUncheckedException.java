package io.codex.service.core.exceptions.base;

import io.codex.service.core.exceptions.error.ErrorCode;
import org.springframework.http.HttpStatus;

public abstract class BaseUncheckedException extends RuntimeException implements BaseException {

    protected ErrorCode errorCode;

    public BaseUncheckedException(String message, ErrorCode errorCode, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public BaseUncheckedException(String message, ErrorCode errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    @Override
    public String getCode() {
        return this.errorCode.getCode();
    }

    @Override
    public HttpStatus getStatus() {
        return this.errorCode.getStatus();
    }

}
