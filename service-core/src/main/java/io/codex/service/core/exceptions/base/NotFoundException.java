package io.codex.service.core.exceptions.base;

import io.codex.service.core.exceptions.error.PlatformErrorCode;
import org.springframework.http.HttpStatus;

public abstract class NotFoundException extends BaseUncheckedException implements BaseException {

    public NotFoundException(String message, Throwable cause) {
        super(message, PlatformErrorCode.PLATFORM_ERR_NOT_FOUND, cause);
    }

    public NotFoundException(String message) {
        super(message, PlatformErrorCode.PLATFORM_ERR_NOT_FOUND);
    }

    @Override
    public String getCode() {
        return super.getCode();
    }

    @Override
    public HttpStatus getStatus() {
        return super.getStatus();
    }

}
