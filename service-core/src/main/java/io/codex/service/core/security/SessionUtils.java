package io.codex.service.core.security;

import org.springframework.security.core.context.SecurityContextHolder;

public class SessionUtils {

    public static CodexUser getUserDetails(){
        return (CodexUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

}
