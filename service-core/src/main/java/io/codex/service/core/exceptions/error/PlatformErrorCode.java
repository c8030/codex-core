package io.codex.service.core.exceptions.error;

import org.springframework.http.HttpStatus;

public enum PlatformErrorCode implements ErrorCode {

    PLATFORM_ERR_DUPLICATE_VALUE("ERR_PLATFORM_4001", HttpStatus.BAD_REQUEST),
    PLATFORM_ERR_NOT_FOUND("ERR_PLATFORM_4041", HttpStatus.NOT_FOUND),
    PLATFORM_ERR_INVALID_INPUT("ERR_PLATFORM_4002", HttpStatus.BAD_REQUEST),
    PLATFORM_ERR_UNKNOWN("ERR_PLATFORM_5001", HttpStatus.INTERNAL_SERVER_ERROR),
    PLATFORM_ERR_FORBIDDEN("ERR_PLATFORM_4031", HttpStatus.FORBIDDEN);

    private final String code;
    private final HttpStatus status;

    PlatformErrorCode(String code, HttpStatus status) {
        this.code = code;
        this.status = status;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public HttpStatus getStatus() {
        return status;
    }
}
