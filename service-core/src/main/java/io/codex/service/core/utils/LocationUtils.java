package io.codex.service.core.utils;

import io.codex.service.dto.core.GeoLocationDTO;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.util.GeometricShapeFactory;

public class LocationUtils {

    public static Geometry createCircleFromCoordinates(GeoLocationDTO location, final double radiusInMeters) {

        double diameterInMeters = 2 * radiusInMeters;

        GeometricShapeFactory shapeFactory = new GeometricShapeFactory();
        shapeFactory.setNumPoints(64); // adjustable
        shapeFactory.setCentre(new Coordinate(location.getLongitude(), location.getLatitude()));
        // Length in meters of 1° of latitude = always 111.32 km
        shapeFactory.setWidth(diameterInMeters/111320d);
        // Length in meters of 1° of longitude = 40075 km * cos( latitude ) / 360
        shapeFactory.setHeight(diameterInMeters / (40075000 * Math.cos(Math.toRadians(location.getLatitude())) / 360));

        return shapeFactory.createEllipse();
    }

}
