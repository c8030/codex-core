package io.codex.service.core.config;

import com.google.gson.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.json.Json;
import springfox.documentation.spring.web.plugins.Docket;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Configuration
public class SwaggerConfig {

    @Value("${maven.artifact.name}")
    private String artifactName;

    @Value("${maven.artifact.description}")
    private String artifactDescription;

    @Value("${maven.artifact.version}")
    private String artifactVersion;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("io.codex.service"))
                .paths(PathSelectors.any())
                .build()
                .securityContexts(Arrays.asList(securityContext()))
                .securitySchemes(Arrays.asList(bearerScheme()));
    }

    private HttpAuthenticationScheme bearerScheme() {
        return new HttpAuthenticationScheme("AuthorizationToken", "Enter your authorization token without 'Bearer' keyword.", "http", "bearer", "JWT", Collections.emptyList());
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())

                .build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Arrays.asList(new SecurityReference("AuthorizationToken", authorizationScopes));
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                artifactName,
                artifactDescription,
                artifactVersion,
                "https://codex-core.com/ichiraku/terms-of-service",
                new Contact("Amit Toor", "https://code-core.com/ichiraku", "amit.toor@gmail.com"),
                "License of API",
                "https://codex-core.com/ichiraku/license",
                Collections.emptyList());
    }

    // Code Below is to make gson compatible with swagger:
    // Remove it if you are not using gson.

    @Bean
    public Gson gson() {
        return new GsonBuilder()
                .registerTypeAdapter(Json.class, new SwaggerJsonTypeAdapter())
                .create();
    }

    public static class SwaggerJsonTypeAdapter implements JsonSerializer<Json> {
        @Override
        public JsonElement serialize(Json json, Type type, JsonSerializationContext context) {
            return new Gson().toJsonTree(json.value());
        }
    }
}
