package io.codex.service.core.exceptions.error;

import lombok.*;

//@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@ToString
public class ErrorDTO {
    private final String errorCode;

    private final String errorMessage;

    public ErrorDTO(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }
}
