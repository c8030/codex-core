package io.codex.service.core.security;

import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@AllArgsConstructor
public class CodexAuthority implements GrantedAuthority {

    private String authority;

    @Override
    public String getAuthority() {
        return null;
    }
}
