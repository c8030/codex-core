package io.codex.service.core.utils;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class CodexOptional<T> {

    private Optional<T> optional;

    private CodexOptional(T t) {
        this.optional = Optional.ofNullable(t);
    }

    public static <T> CodexOptional<T> ofNullable(T t) {
        return new CodexOptional<T>(t);
    }

    public CodexElseOptional ifPresent(Consumer<T> consumer) {
        optional.ifPresent(consumer);
        return this.new CodexElseOptional();
    }

    public class CodexElseOptional {

        private CodexElseOptional() {}

        public void orElse(Runnable r) {
            if (!optional.isPresent()) {
                r.run();
            }
        }

        public <X extends Throwable> T orElseThrow(Supplier<? extends X> supplier) throws X {
            return optional.orElseThrow(supplier);
        }
    }
}
