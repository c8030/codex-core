package io.codex.service.core.context;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class RequestContext {

    private String correlationId;

    public static RequestContext of(String correlationId){
        return new RequestContext(correlationId);
    }
}
