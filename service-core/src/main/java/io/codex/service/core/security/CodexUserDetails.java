package io.codex.service.core.security;

import org.springframework.security.core.userdetails.UserDetails;

public interface CodexUserDetails extends UserDetails {

    Long getUserId();

    Long getTenantId();

    String getTenantLogin();

    String getName();
}
