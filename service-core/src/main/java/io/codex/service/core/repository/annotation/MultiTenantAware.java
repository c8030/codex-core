package io.codex.service.core.repository.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is designed for Spring data repositories.
 * If any repository is marked with this annotation, all methods
 * within that repository will respect tenancy.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface MultiTenantAware {
}
