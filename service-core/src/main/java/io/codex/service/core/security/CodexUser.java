package io.codex.service.core.security;

import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.List;

@AllArgsConstructor
@SuperBuilder
public class CodexUser implements CodexUserDetails{

    private Long tenantId;

    private String tenantLogin;

    private Long userId;

    private String username;

    private String fullName;

    private List<CodexAuthority> authorities;

    @Override
    public Long getUserId() {
        return userId;
    }

    @Override
    public Long getTenantId() {
        return this.tenantId;
    }

    @Override
    public String getTenantLogin() {
        return this.tenantLogin;
    }

    @Override
    public String getName() {
        return this.fullName;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return  this.authorities;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
