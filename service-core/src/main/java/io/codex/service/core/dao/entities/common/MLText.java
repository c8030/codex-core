package io.codex.service.core.dao.entities.common;

import io.codex.service.core.dao.entities.base.BaseEntity;
import lombok.Data;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;

@Entity(name = "mltext")
@Data
@FilterDef(name="languageFilter", parameters= {
        @ParamDef(name="languageCode", type="string" )
})
@Filter(name = "languageFilter", condition = "languageCode = :languageCode")
public class MLText extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String languageCode;

    @Column(nullable = false, length =512)
    private String text;

}
