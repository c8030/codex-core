package io.codex.service.core.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClaims;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.function.Function;

public class JwtUtil{

    public static String extractUsername(String token){
        return extractClaim(token, Claims::getSubject);
    }

    public static Long extractTenantId(String token){
         return new DefaultClaims().get("tenantId", Long.class);
    }

    public static Boolean isTokenExpired(String token){
        return extractClaim(token, Claims::getExpiration).before(new Date(Instant.now().toEpochMilli()));
    }

    public static Boolean validateToken(String token){
        return !isTokenExpired(token);
    }

    public static <T> T extractClaim(String token, Function<Claims,T> claimResolver){
        return claimResolver.apply(extractAllClaims(token));
    }

    public static Claims extractAllClaims(String token){
        return Jwts.parser().setSigningKey(SecurityConstants.KEY).parseClaimsJws(token).getBody();
    }

    public static String generateToken(CodexUserDetails userDetails){
        return Jwts.builder()
                .setIssuer("CodexAuthService")
                .setSubject(userDetails.getUsername())
                .claim("fullName", userDetails.getName())
                .claim("authorities", userDetails.getAuthorities())
                .claim("tenantId",userDetails.getTenantId())
                .claim("tenantLogin", userDetails.getTenantLogin())
                .claim("userId",userDetails.getUserId())
                .setIssuedAt(Date.from(Instant.now()))
                .setExpiration(Date.from(Instant.now().plus(SecurityConstants.EXPIRATION_TIME, ChronoUnit.MINUTES)))
                .signWith(SignatureAlgorithm.HS512, SecurityConstants.KEY)
                .compact();
    }

    public static CodexUser getUserFromToken(String token){
        // Get All claims
        final Claims allClaims = extractAllClaims(token);

        return CodexUser.builder()
                .userId(allClaims.get("userId", Long.class))
                .username(allClaims.getSubject())
                .fullName(allClaims.get("fullName",String.class))
                .tenantId(allClaims.get("tenantId", Long.class))
                .tenantLogin(allClaims.get("tenantLogin", String.class))
                .authorities((List<CodexAuthority>) allClaims.get("authorities" ))
                .build();
    }

}
