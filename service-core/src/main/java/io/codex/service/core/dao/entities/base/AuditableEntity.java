package io.codex.service.core.dao.entities.base;


import io.codex.service.core.security.SessionUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Slf4j
@Data
@NoArgsConstructor
@SuperBuilder
@MappedSuperclass
public abstract class AuditableEntity implements Serializable {

    @NotNull
    protected Long createdUser;

    protected Long lastUpdatedUser;

    @NotNull
    protected LocalDateTime createdDateTime;

    protected LocalDateTime updatedDateTime;

    @PrePersist
    public void setAuditFields() {
        log.trace("AuditableEntity: Setting setAuditFields");
        this.createdUser = SessionUtils.getUserDetails().getUserId();
        this.createdDateTime = this.createdDateTime = LocalDateTime.now(ZoneOffset.UTC);
    }

    @PreUpdate
    public void updateAuditFields() {
        log.trace("AuditableEntity: Setting updateAuditFields");
        this.lastUpdatedUser = SessionUtils.getUserDetails().getUserId();
        this.updatedDateTime = LocalDateTime.now(ZoneOffset.UTC);
    }
}
