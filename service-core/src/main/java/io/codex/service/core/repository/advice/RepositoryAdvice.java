package io.codex.service.core.repository.advice;

import io.codex.service.core.security.SessionUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Aspect
@Component
@Slf4j
public class RepositoryAdvice {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * This enables tenant filter for current session for all entities that are extending AuditableMTEntity class.
     * @param joinPoint
     */
    @Before("isRepository() && isMultiTenantAware()")
    public void addTenantFilter(JoinPoint joinPoint){
        log.trace("RepositoryAdvice: Adding Tenant Filter.");
        if (entityManager.isOpen()) {
            Session session = entityManager.unwrap(Session.class);
            session.enableFilter("tenantFilter").setParameter("tenantId", SessionUtils.getUserDetails().getTenantId());
        }
    }

    @Pointcut("within(org.springframework.data.repository.Repository+)")
    public void isRepository() {}

    @Pointcut("@within(io.codex.service.core.repository.annotation.MultiTenantAware) || @annotation(io.codex.service.core.repository.annotation.MultiTenantAware)")
    public void isMultiTenantAware(){}

}
