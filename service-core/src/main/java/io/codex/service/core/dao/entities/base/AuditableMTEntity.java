package io.codex.service.core.dao.entities.base;


import io.codex.service.core.security.SessionUtils;
import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Slf4j
@MappedSuperclass
@Data
@NoArgsConstructor
@SuperBuilder
@FilterDef(name = "tenantFilter", parameters = {
        @ParamDef(name = "tenantId", type = "long")
})
@Filter(name = "tenantFilter", condition = "tenant = :tenantId")
public abstract class AuditableMTEntity implements Serializable {

    @NotNull
    protected Long tenant;

    @NotNull
    protected Long createdTenant;

    protected Long updatedTenant;

    @NotNull
    protected Long createdUser;

    protected Long lastUpdatedUser;

    @NotNull
    protected LocalDateTime createdDateTime;

    protected LocalDateTime updatedDateTime;


    @PrePersist
    public void setAuditFields() {
        log.trace("AuditableMTEntity: Setting setAuditFields");

        this.createdTenant = SessionUtils.getUserDetails().getTenantId();
        this.tenant = SessionUtils.getUserDetails().getTenantId();
        this.createdUser = SessionUtils.getUserDetails().getUserId();
        this.createdDateTime = LocalDateTime.now(ZoneOffset.UTC);
    }

    @PreUpdate
    public void updateAuditFields() {
        log.trace("AuditableMTEntity: Setting updateAuditFields");

        this.lastUpdatedUser = SessionUtils.getUserDetails().getUserId();
        this.updatedTenant = SessionUtils.getUserDetails().getTenantId();
        this.updatedDateTime = LocalDateTime.now(ZoneOffset.UTC);
    }
}
