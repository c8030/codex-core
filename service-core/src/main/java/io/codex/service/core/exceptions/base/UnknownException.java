package io.codex.service.core.exceptions.base;

import io.codex.service.core.exceptions.error.PlatformErrorCode;
import org.springframework.http.HttpStatus;

public abstract class UnknownException extends BaseUncheckedException implements BaseException {

    public UnknownException(String message, Throwable cause) {
        super(message, PlatformErrorCode.PLATFORM_ERR_UNKNOWN, cause);
    }

    public UnknownException(String message) {
        super(message, PlatformErrorCode.PLATFORM_ERR_UNKNOWN);
    }

    @Override
    public String getCode() {
        return super.getCode();
    }

    @Override
    public HttpStatus getStatus() {
        return super.getStatus();
    }

}
