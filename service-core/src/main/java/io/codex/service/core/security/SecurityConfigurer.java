package io.codex.service.core.security;

import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
@Order(1)
public class SecurityConfigurer extends WebSecurityConfigurerAdapter {

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        super.configure(auth);
//    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/api/users/authenticate/**")
                .antMatchers("/v3/api-docs",
                        "/swagger-ui.html",
                        "/swagger-resources/**",
                        "**/swagger-ui/**")
                .antMatchers("/api/tenants/register/**")
                .antMatchers("/api/service/console/**");
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/api/users/authenticate/**",
                        "/v3/api-docs",
                        "/swagger-ui.html",
                        "/swagger-resources/**",
                        "**/swagger-ui/**",
                        "/api/tenants/register/**",
                        "/api/service/console/**")
                .permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(new AuthenticationFilter(),UsernamePasswordAuthenticationFilter.class);
    }
}
