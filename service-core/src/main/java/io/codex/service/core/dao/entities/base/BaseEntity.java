package io.codex.service.core.dao.entities.base;

import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;

import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;


@Slf4j
@Data
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

    @NotNull
    protected LocalDateTime createdDateTime;

    protected LocalDateTime updatedDateTime;

    @PrePersist
    public void setCreatedDateTime() {
        log.trace("BaseEntity: Setting CreatedDateTime");
        this.createdDateTime = LocalDateTime.now(ZoneOffset.UTC);
    }

    @PreUpdate
    public void setUpdatedDateTime() {

        log.trace("BaseEntity: Setting UpdatedDateTime");
        this.updatedDateTime = LocalDateTime.now(ZoneOffset.UTC);
    }
}
