package io.codex.service.core.exceptions.base;

import io.codex.service.core.exceptions.error.PlatformErrorCode;
import org.springframework.http.HttpStatus;

public abstract class InvalidInputException extends BaseUncheckedException implements BaseException {

    public InvalidInputException(String message, Throwable cause) {
        super(message, PlatformErrorCode.PLATFORM_ERR_INVALID_INPUT, cause);
    }

    public InvalidInputException(String message) {
        super(message, PlatformErrorCode.PLATFORM_ERR_INVALID_INPUT);
    }

    @Override
    public String getCode() {
        return super.getCode();
    }

    @Override
    public HttpStatus getStatus() {
        return super.getStatus();
    }

}
