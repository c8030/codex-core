package io.codex.service.core.exceptions.base;

import org.springframework.http.HttpStatus;

public interface BaseException {

   String getCode();

   HttpStatus getStatus();

   default String getMessage(Throwable e) {
       return e.getMessage();
   }

}
