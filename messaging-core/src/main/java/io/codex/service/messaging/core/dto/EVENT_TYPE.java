package io.codex.service.messaging.core.dto;

public enum EVENT_TYPE {

    DOMAIN_EVENT("DOMAIN_EVENT"),
    COMMAND("COMMAND");

    private String name;

    private EVENT_TYPE(String name){
        this.name = name;
    };


}
