package io.codex.service.messaging.core.logminer.impl;

import com.github.shyiko.mysql.binlog.BinaryLogClient;
import com.github.shyiko.mysql.binlog.event.EventType;
import com.github.shyiko.mysql.binlog.event.TableMapEventData;
import com.github.shyiko.mysql.binlog.event.UpdateRowsEventData;
import com.github.shyiko.mysql.binlog.event.WriteRowsEventData;
import io.codex.service.messaging.core.logminer.LogMiner;
import io.codex.service.messaging.core.manager.OutboxProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
public class MySQLLogMiner implements LogMiner {

    @Value("${spring.datasource.hostname}")
    private String hostname;

    @Value("${spring.datasource.name}")
    private String schema;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;

    @Autowired
    private OutboxProcessor outboxProcessor;

    private final String outboxTableName ="MESSAGE_OUTBOX";

    Long tableId = 0L;

    @Override
    public void mineDatabaseLogs() {

        String host = hostname.split(":")[0];
        Integer port = Integer.parseInt(hostname.split(":")[1]);

        BinaryLogClient client =
                new BinaryLogClient(host, port, schema, username, password);

        client.registerEventListener(event -> {
            if( event.getHeader().getEventType().equals(EventType.TABLE_MAP)
                    && outboxTableName.equalsIgnoreCase(((TableMapEventData) event.getData()).getTable())){
                tableId = ((TableMapEventData) event.getData()).getTableId();
            }

            if( event.getHeader().getEventType().equals(EventType.EXT_WRITE_ROWS)
                    && tableId.equals(((WriteRowsEventData) event.getData()).getTableId())){
                triggerOutboxProcessor();
            }
        });

        try {
            client.connect();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }


    private void triggerOutboxProcessor(){
        outboxProcessor.processOutbox();
    }

}
