package io.codex.service.messaging.core.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import lombok.Value;

import java.util.Map;

@Builder
@Getter
public class Message<T> {

    private String identifier;

    @Singular
    private Map<MessageHeader,String> headers;

    private T payload;

    public T getPayload() {
        return payload;
    }

    public String getIdentifier() {
        return identifier;
    }


}
