package io.codex.service.messaging.core.relayer.annotations;

import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is designed for Handler classes for outbox message.
 * If a class is marked with this annotation. Then it will be searched
 * by router to route message to appropriate message relayer.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Component
public @interface OutboxMessageHandler {

    String aggregate();

}
