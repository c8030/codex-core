package io.codex.service.messaging.core;

import io.codex.service.messaging.core.config.ProducerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.ProducerFactory;

@Configuration
public class CodexMessenger {

    @Autowired
    private ProducerConfig  producerConfig;

    @Bean
    public ProducerFactory producerFactory(){
        return new DefaultKafkaProducerFactory<>(producerConfig.senderProps());
    }

}
