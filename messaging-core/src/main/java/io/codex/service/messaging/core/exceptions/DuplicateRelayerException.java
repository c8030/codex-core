package io.codex.service.messaging.core.exceptions;

public class DuplicateRelayerException extends RuntimeException{

    public DuplicateRelayerException() {
        super();
    }

    public DuplicateRelayerException(String message) {
        super(message);
    }

    public DuplicateRelayerException(String message, Throwable cause) {
        super(message, cause);
    }
}
