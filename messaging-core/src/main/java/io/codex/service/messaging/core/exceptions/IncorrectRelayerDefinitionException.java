package io.codex.service.messaging.core.exceptions;

public class IncorrectRelayerDefinitionException extends RuntimeException{

    public IncorrectRelayerDefinitionException() {
        super();
    }

    public IncorrectRelayerDefinitionException(String message) {
        super(message);
    }

    public IncorrectRelayerDefinitionException(String message, Throwable cause) {
        super(message, cause);
    }
}
