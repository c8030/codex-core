package io.codex.service.messaging.core.manager;

import com.google.gson.Gson;
import io.codex.service.messaging.core.config.ServiceMessagingConfig;
import io.codex.service.messaging.core.dto.EVENT_TYPE;
import io.codex.service.messaging.core.dto.Message;
import io.codex.service.messaging.core.dto.MessageHeader;
import io.codex.service.messaging.core.exceptions.OutboxProcessException;
import io.codex.service.messaging.core.model.MESSAGE_OUTBOX;
import io.codex.service.messaging.core.model.repo.MessageOutboxRepo;
import io.codex.service.messaging.core.router.MessageRouter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class OutboxProcessor {

    @Value("${spring.application.name}")
    private String serviceName;

    @Autowired
    private MessageOutboxRepo messageOutboxRepo;

    @Autowired
    private MessageRouter messageRouter;

    public void processOutbox(){
        messageOutboxRepo.getOutboxItems(ServiceMessagingConfig.serviceInstanceId)
                .forEach(outboxMsg -> sendToMessageRelayer(outboxMsg));
    }

    private void sendToMessageRelayer(MESSAGE_OUTBOX outboxLineItem){
        messageRouter.route(outboxLineItem.getAggregate(),outboxLineItem.getMessageTopic(),convertToMessage(outboxLineItem));
    }

    private Message convertToMessage(MESSAGE_OUTBOX outboxLineItem){
        try {
            return Message.builder()
                    .header(MessageHeader.CORRELATION_ID, UUID.randomUUID().toString())
                    .header(MessageHeader.EVENT_TYPE, EVENT_TYPE.DOMAIN_EVENT.name())
                    .header(MessageHeader.FROM_SERVICE, serviceName)
                    .identifier(UUID.randomUUID().toString())
                    .payload(new Gson().fromJson(outboxLineItem.getMessage(), Class.forName(outboxLineItem.getMessageClass())))
                    .build();
        } catch (ClassNotFoundException e) {
            throw new OutboxProcessException(e.getMessage(),e);
        }
    }

}
