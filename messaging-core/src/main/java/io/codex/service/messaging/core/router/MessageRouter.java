package io.codex.service.messaging.core.router;

import io.codex.service.messaging.core.dto.Message;
import io.codex.service.messaging.core.exceptions.DuplicateRelayerException;
import io.codex.service.messaging.core.exceptions.IncorrectRelayerDefinitionException;
import io.codex.service.messaging.core.logminer.LogMiner;
import io.codex.service.messaging.core.relayer.annotations.MessageRelayer;
import io.codex.service.messaging.core.relayer.annotations.OutboxMessageHandler;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

@Component("messageRouter")
@Slf4j
public class MessageRouter {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private LogMiner logMiner;

    private Map<String,Map<String, RelayerFunctionalInterface>> relayerReferences;

    private final String RELAYER_METHOD_FIRST_ARGUMENT_TYPE = "java.lang.String";

    private final String RELAYER_METHOD_SECOND_ARGUMENT_TYPE = "io.codex.service.messaging.core.dto.Message";


    /**
     * Create routing tables(by storing method references) when all beans have been initialized.
     * @param event
     */
//    @EventListener
//    public void init(ContextRefreshedEvent event){
//        relayerReferences = new HashMap<>();
//        //Scan all MessageRelayers and store method references
//        Set<Class<?>> outboxHandlers = new Reflections("io.codex.service").getTypesAnnotatedWith(OutboxMessageHandler.class);
//        processClasses(outboxHandlers);
//
//        //Start Log Mining in other Thread
//        new Thread(()-> logMiner.mineDatabaseLogs()).start();
//    }

    /**
     * @param aggregate
     * @param topic
     * @param message
     */
    public void route(String aggregate, String topic, Message message){
        relayerReferences.get(aggregate).get(topic).relay(topic, message);
    }

    private void processClasses(Set<Class<?>> outboxHandlers){
        outboxHandlers.forEach(aClass -> storeMethodReferences(aClass));
    }

    private void storeMethodReferences(Class<?> outboxHandler){
        String aggregateName = outboxHandler.getDeclaredAnnotation(OutboxMessageHandler.class).aggregate();

        if(!relayerReferences.containsKey(aggregateName)){
            relayerReferences.put(aggregateName, new HashMap<>());
        }

        List<Method> methods = Arrays.stream(outboxHandler.getDeclaredMethods())
                .filter(method -> method.isAnnotationPresent(MessageRelayer.class))
                .collect(Collectors.toList());

        Map<String, RelayerFunctionalInterface> methodReferences = relayerReferences.get(aggregateName);

        methods.forEach(method -> validateAndStoreMethod(methodReferences, method));
    }


    private void executeMethod(Method method, String topic, Message<Object> message){
        // Get Spring Managed Bean and Execute Method from reference.
        try {
            method.invoke(applicationContext.getBean(method.getDeclaringClass()), topic, message);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void validateAndStoreMethod(Map<String, RelayerFunctionalInterface> topicMethodMap, Method method){

        String methodTopicName = method.getDeclaredAnnotation(MessageRelayer.class).topic();

        // If topic, method already stored.
        if(topicMethodMap.containsKey(methodTopicName)){
            throw new DuplicateRelayerException(String.format("Found more than 1 implementation for topic %s, needed one.", methodTopicName));
        }

        // Verify Method Signature
        if(!validMethodSignature(method)){
            throw new IncorrectRelayerDefinitionException(String.format("Incorrect Relayer Method definition for %s, should be (String topic, Message<T> message)", methodTopicName));
        }

        topicMethodMap.put(methodTopicName, (topic, message) -> executeMethod(method, topic, message));

    }

    private boolean validMethodSignature(Method method) {
        if(method.getParameterTypes().length != 2){
            return false;
        }

        String argument1Type = method.getParameterTypes()[0].getTypeName();
        String argument2Type = method.getParameterTypes()[1].getTypeName();

        return argument1Type.equals(RELAYER_METHOD_FIRST_ARGUMENT_TYPE) && argument2Type.equals(RELAYER_METHOD_SECOND_ARGUMENT_TYPE);
    }
}
