package io.codex.service.messaging.core.exceptions;

public class OutboxProcessException extends RuntimeException{

    public OutboxProcessException() {
        super();
    }

    public OutboxProcessException(String message) {
        super(message);
    }

    public OutboxProcessException(String message, Throwable cause) {
        super(message, cause);
    }
}
