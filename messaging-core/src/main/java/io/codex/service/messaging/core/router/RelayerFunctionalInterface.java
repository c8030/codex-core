package io.codex.service.messaging.core.router;

import io.codex.service.messaging.core.dto.Message;
import org.apache.kafka.common.protocol.types.Field;

@FunctionalInterface
public interface RelayerFunctionalInterface {

    public void relay(String topic, Message message);

}
