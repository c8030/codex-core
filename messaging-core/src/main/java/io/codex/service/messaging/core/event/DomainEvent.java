package io.codex.service.messaging.core.event;

import com.google.gson.Gson;
import io.codex.service.messaging.core.config.ServiceMessagingConfig;
import io.codex.service.messaging.core.model.MESSAGE_OUTBOX;
import io.codex.service.messaging.core.model.repo.MessageOutboxRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class DomainEvent {

    @Autowired
    private MessageOutboxRepo messageOutboxRepo;

    public void publish(String aggregate, String topic, Object obj){
        messageOutboxRepo.save(MESSAGE_OUTBOX.builder()
                .aggregate(aggregate)
                .messageTopic(topic)
                .message(new Gson().toJson(obj))
                .messageClass(obj.getClass().getTypeName())
                .serviceInstance(ServiceMessagingConfig.serviceInstanceId)
                .build());
    }

}
