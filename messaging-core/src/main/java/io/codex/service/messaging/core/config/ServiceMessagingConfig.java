package io.codex.service.messaging.core.config;

import org.springframework.context.annotation.Configuration;

import java.util.UUID;

@Configuration
public class ServiceMessagingConfig {

    public final static String serviceInstanceId = UUID.randomUUID().toString();

}
