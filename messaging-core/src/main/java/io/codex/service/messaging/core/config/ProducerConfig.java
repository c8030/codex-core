package io.codex.service.messaging.core.config;

import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

import static org.apache.kafka.clients.producer.ProducerConfig.*;

@Configuration
public class ProducerConfig {

    @Value("${kafka.broker.hostname:localhost}")
    private String kafkaHost;

    @Value("${kafka.broker.port:port}")
    private String kafkaPort;

    @Value("${kafka.schema-registry.hostname:localhost}")
    private String schemaRegistryHost;

    @Value("${kafka.schema-registry.port:8081}")
    private String schemaRegistryPort;

    public Map<String, Object> senderProps() {
        Map<String, Object> props = new HashMap<>();
        props.put(BOOTSTRAP_SERVERS_CONFIG, kafkaHost + ":" + kafkaPort);
        props.put(RETRIES_CONFIG, 10);
        props.put(BATCH_SIZE_CONFIG, 16384);
        props.put(LINGER_MS_CONFIG, 1);
        props.put("schema.registry.url", schemaRegistryHost + ":" + schemaRegistryPort);
        props.put(KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);
        return props;
    }

}
