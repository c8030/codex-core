package io.codex.service.messaging.core.relayer.annotations;

import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;

/**
 * This annotation is designed for methods responsible for sending messages from outbox to message broker.
 * A method if annotated with this will receive all messages for specified topics and aggregate defined
 * under OutboxMessageHandlers annotation at class level.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface MessageRelayer {

    String topic();

}
