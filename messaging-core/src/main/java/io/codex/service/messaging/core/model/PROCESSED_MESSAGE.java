package io.codex.service.messaging.core.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import java.time.Instant;

@Entity(name= "PROCESSED_MESSAGE")
@Data
public class PROCESSED_MESSAGE {

    @Id
    @Column(name="message_identifier")
    private String messageIdentifier;

    @Column(name="processed_time", nullable = false, updatable = false)
    private Long processedTime;

    @PrePersist
    public void prePersist() {
        this.processedTime = Instant.now().toEpochMilli();
    }
}
