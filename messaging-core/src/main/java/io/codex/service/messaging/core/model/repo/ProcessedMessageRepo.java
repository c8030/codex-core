package io.codex.service.messaging.core.model.repo;

import io.codex.service.messaging.core.model.PROCESSED_MESSAGE;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProcessedMessageRepo extends JpaRepository<PROCESSED_MESSAGE, String> {
}
