package io.codex.service.messaging.core.model;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;

@Entity(name= "MESSAGE_OUTBOX")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MESSAGE_OUTBOX {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="aggregate", nullable = false, updatable = false)
    private String aggregate;

    @Column(name="message_topic", nullable = false, updatable = false)
    private String messageTopic;

    @Column(name="message", updatable = false)
    private String message;

    @Column(name="messageClass", updatable = false)
    private String messageClass;

    @Column(name = "created_at", nullable = false, updatable = false)
    private Long createdAt;

    @Column(name = "service_instance", nullable = false, updatable = false)
    private String serviceInstance;

    @PrePersist
    public void prePersist() {
        this.createdAt = Instant.now().toEpochMilli();
    }
}
