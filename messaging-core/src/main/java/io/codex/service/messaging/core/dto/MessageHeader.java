package io.codex.service.messaging.core.dto;

public enum MessageHeader {

    MESSAGE_KEY("message-key"),
    CORRELATION_ID("correlation-id"),
    EVENT_TYPE("event-type"),
    FROM_SERVICE("sender-service");

    private String name;

    private MessageHeader(String name){
        this.name = name;
    };


}
