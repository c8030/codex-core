package io.codex.service.messaging.core.model.repo;

import io.codex.service.messaging.core.model.MESSAGE_OUTBOX;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageOutboxRepo extends JpaRepository<MESSAGE_OUTBOX, Long> {

    @Query("Select msg from MESSAGE_OUTBOX msg where msg.serviceInstance = :service_instance_id")
    public List<MESSAGE_OUTBOX> getOutboxItems(@Param("service_instance_id") String serviceInstanceId);

}
